package pro.salomatov.srss.service;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import pro.salomatov.srss.R;
import pro.salomatov.srss.WidgetProvider;
import pro.salomatov.srss.activity.SettingsActivity;
import pro.salomatov.srss.util.RSSItem;

/**
 * Created by PAVEL on 14.10.2015.
 */
public class UpdateWidgetService extends Service {

    private static final String TAG = UpdateWidgetService.class.getSimpleName();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "UpdateWidgetService onStartCommand() called");
        int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);

        }
        if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this
                    .getApplicationContext());
            final SharedPreferences sharedPreferences = getSharedPreferences(
                    SettingsActivity.WIDGET_SETTINGS, Context.MODE_PRIVATE);
            RemoteViews remoteViews = new RemoteViews(this
                    .getApplicationContext().getPackageName(),
                    R.layout.layout_widget);
            Gson gson = new Gson();
            String json = sharedPreferences.getString(SettingsActivity.PREF_RSS_FEED + widgetId, null);
            if(json != null){
                //Log.d(TAG, "Json " + json.toString());
                Type type = new TypeToken<List<RSSItem>>(){}.getType();
                List<RSSItem> rssItemList = gson.fromJson(json, type);
                int rssPage = sharedPreferences.getInt(SettingsActivity.PREF_RSS_PAGE + widgetId, -1);
                Log.d(TAG, "rssPage " + rssPage);
                if (rssPage != -1){

                    if (rssPage == 0){
                        remoteViews.setBoolean(R.id.btn_next_rss, "setEnabled", false);
                        remoteViews.setBoolean(R.id.btn_prev_rss, "setEnabled", true);
                    }else if (rssPage == rssItemList.size() - 1){
                        remoteViews.setBoolean(R.id.btn_next_rss, "setEnabled", true);
                        remoteViews.setBoolean(R.id.btn_prev_rss, "setEnabled", false);
                    }else{
                        remoteViews.setBoolean(R.id.btn_next_rss, "setEnabled", true);
                        remoteViews.setBoolean(R.id.btn_prev_rss, "setEnabled", true);
                    }
                    remoteViews.setTextViewText(R.id.txt_rss_title, rssItemList.get(rssPage).getTitle());
                    remoteViews.setTextViewText(R.id.txt_rss_text, rssItemList.get(rssPage).getDescription());
                }
            }else{
                remoteViews.setTextViewText(R.id.txt_rss_title, getResources().getString(R.string.error));
                remoteViews.setTextViewText(R.id.txt_rss_text, "");
                remoteViews.setBoolean(R.id.btn_prev_rss, "setEnabled", false);
                remoteViews.setBoolean(R.id.btn_next_rss, "setEnabled", false);
            }

            //remoteViews.setTextViewText(R.id.txt_rss_title, sharedPreferences.getString(SettingsActivity.PREF_RSS_ADDRESS + widgetId, ""));

            //

            Intent prevIntent = new Intent(this.getApplicationContext(), WidgetProvider.class);
            prevIntent.setAction(WidgetProvider.ACTION_PREV);
            prevIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    getApplicationContext(), widgetId, prevIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.btn_prev_rss, pendingIntent);

            Intent nextIntent = new Intent(this.getApplicationContext(), WidgetProvider.class);
            nextIntent.setAction(WidgetProvider.ACTION_NEXT);
            nextIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            pendingIntent = PendingIntent.getBroadcast(
                    getApplicationContext(), widgetId, nextIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.btn_next_rss, pendingIntent);

            Intent settingsIntent = new Intent(this.getApplicationContext(), SettingsActivity.class);
            settingsIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            settingsIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            pendingIntent = PendingIntent.getActivity(
                    getApplicationContext(), widgetId, settingsIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.btn_widget_prefs, pendingIntent);

            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
        stopSelf();
        return START_NOT_STICKY;
    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
