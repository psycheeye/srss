package pro.salomatov.srss;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import com.google.gson.Gson;
import com.pkmmte.pkrss.Article;
import com.pkmmte.pkrss.Callback;
import com.pkmmte.pkrss.PkRSS;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import pro.salomatov.srss.activity.SettingsActivity;
import pro.salomatov.srss.service.UpdateWidgetService;
import pro.salomatov.srss.util.RSSItem;

/**
 * Created by PAVEL on 13.10.2015.
 */
public class WidgetProvider extends AppWidgetProvider {

    private static final String TAG = WidgetProvider.class.getSimpleName();
    public static final String ACTION_NEXT = "pro.salomatov.srss.ACTION_NEXT";
    public static final String ACTION_PREV = "pro.salomatov.srss.ACTION_PREV";
    public static final String ACTION_LOADED = "pro.salomatov.srss.ACTION_LOADED";

    @Override
    public void onEnabled(Context context) {
        Log.d(TAG, "onEnabled() called");
        super.onEnabled(context);
        startUpdate(context);

    }

    @Override
    public void onDisabled(Context context) {
        Log.d(TAG, "onDisabled() called");
        super.onDisabled(context);
        cancelUpdate(context);

    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        for (int widgetId:appWidgetIds){
            SharedPreferences.Editor editor = context.getSharedPreferences(SettingsActivity.WIDGET_SETTINGS,
                    Context.MODE_PRIVATE).edit();
            editor.remove(SettingsActivity.PREF_RSS_ADDRESS + widgetId);
            editor.remove(SettingsActivity.PREF_RSS_FEED + widgetId);
            editor.remove(SettingsActivity.PREF_RSS_PAGE + widgetId);
            editor.apply();
        }

    }

    @Override
    public void onUpdate(final Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        Log.d(TAG, "onUpdate() called");
        //updating every instance of widget
        ComponentName thisWidget = new ComponentName(context,
                WidgetProvider.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        SharedPreferences sharedPreferences = context.getSharedPreferences(SettingsActivity.WIDGET_SETTINGS, Context.MODE_PRIVATE);
        for (final int widgetId : allWidgetIds) {
            String address = sharedPreferences.getString(SettingsActivity.PREF_RSS_ADDRESS + widgetId, "");
            if (!address.isEmpty()){
                PkRSS.with(context).load(address).callback(new Callback() {
                    @Override
                    public void OnPreLoad() {

                    }

                    @Override
                    public void OnLoaded(List<Article> articleList) {
                        SharedPreferences sharedPreferences = context.getSharedPreferences(SettingsActivity.WIDGET_SETTINGS, Context.MODE_PRIVATE);
                        if(articleList.isEmpty()){
                            sharedPreferences.edit()
                                    .remove(SettingsActivity.PREF_RSS_FEED + widgetId)
                                    .remove(SettingsActivity.PREF_RSS_PAGE + widgetId)
                                    .apply();
                        }else{
                            Gson gson = new Gson();
                            List<RSSItem> rssItemList = new ArrayList<>();
                            for (Article article: articleList){
                                rssItemList.add(new RSSItem(article.getTitle(), article.getDescription()));
                            }
                            String json = gson.toJson(rssItemList);
                            sharedPreferences.edit()
                                    .putString(SettingsActivity.PREF_RSS_FEED + widgetId, json)
                                    .putInt(SettingsActivity.PREF_RSS_PAGE + widgetId, 0)
                                    .apply();
                        }
                        Intent intent = new Intent(context, WidgetProvider.class);
                        intent.setAction(ACTION_LOADED);
                        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
                        context.sendBroadcast(intent);
                    }

                    @Override
                    public void OnLoadFailed() {
                    }
                }).async();
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() called " + intent.getAction());
        if(intent.getAction().equalsIgnoreCase(AppWidgetManager.ACTION_APPWIDGET_UPDATE)){
            //manually call onUpdate(), because if intent has empty EXTRA_APPWIDGET_IDS, onUpdate() never called
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            this.onUpdate(context, appWidgetManager, null);
        }else if (intent.getAction().equalsIgnoreCase(ACTION_PREV)) {

            int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
            Bundle extras = intent.getExtras();
            if (extras != null) {
                widgetId = extras.getInt(
                        AppWidgetManager.EXTRA_APPWIDGET_ID,
                        AppWidgetManager.INVALID_APPWIDGET_ID);

            }
            if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
                Log.d(TAG, "onReceive() prev " + widgetId);
                SharedPreferences sharedPreferences = context.getSharedPreferences(SettingsActivity.WIDGET_SETTINGS, Context.MODE_PRIVATE);
                int rssPage = sharedPreferences.getInt(SettingsActivity.PREF_RSS_PAGE + widgetId, -1);
                if (rssPage != -1){
                    sharedPreferences.edit().putInt(SettingsActivity.PREF_RSS_PAGE + widgetId, rssPage + 1).apply();
                    Intent updateIntent = new Intent(context, UpdateWidgetService.class);
                    updateIntent.putExtras(intent);
                    context.startService(updateIntent);
                }

            }
        }else if (intent.getAction().equalsIgnoreCase(ACTION_NEXT)) {

            int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
            Bundle extras = intent.getExtras();
            if (extras != null) {
                widgetId = extras.getInt(
                        AppWidgetManager.EXTRA_APPWIDGET_ID,
                        AppWidgetManager.INVALID_APPWIDGET_ID);

            }
            if (widgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
                Log.d(TAG, "onReceive() next " + widgetId);
                SharedPreferences sharedPreferences = context.getSharedPreferences(SettingsActivity.WIDGET_SETTINGS, Context.MODE_PRIVATE);
                int rssPage = sharedPreferences.getInt(SettingsActivity.PREF_RSS_PAGE + widgetId, -1);
                if (rssPage > 0){
                    sharedPreferences.edit().putInt(SettingsActivity.PREF_RSS_PAGE + widgetId, rssPage - 1).apply();
                    Intent updateIntent = new Intent(context, UpdateWidgetService.class);
                    updateIntent.putExtras(intent);
                    context.startService(updateIntent);
                }
            }
        }else if (intent.getAction().equalsIgnoreCase(ACTION_LOADED)){
            Intent updateIntent = new Intent(context, UpdateWidgetService.class);
            updateIntent.putExtras(intent);
            context.startService(updateIntent);
        }else {
            Log.d(TAG, "call super.onReceive()");
            super.onReceive(context, intent);
        }
    }


    /**
     * Start repeating AlarmManager to broadcasting widget update intent every minute
     * Called then 1st instance of widget is created
     * @param context
     */

    public static void startUpdate(Context context) {
        final Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());

        Intent intent = new Intent(context, WidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC, c.getTimeInMillis(),
                1000 * 60, pendingIntent);

    }

    /**
     * Stop repeating AlarmManager to broadcasting widget update intent every minute
     * Called then last instance of widget deleted
     * @param context
     */

    public static void cancelUpdate(Context context) {

        Intent intent = new Intent(context, WidgetProvider.class);

        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

    }
}
