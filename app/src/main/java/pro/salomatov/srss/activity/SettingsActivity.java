package pro.salomatov.srss.activity;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import java.util.regex.Pattern;
import pro.salomatov.srss.R;
import pro.salomatov.srss.WidgetProvider;

/**
 * Created by PAVEL on 13.10.2015.
 */
public class SettingsActivity extends AppCompatActivity {

    public static final String WIDGET_SETTINGS = "widget_settings";
    public static final String PREF_RSS_ADDRESS = "pref_rss_address";
    public static final String PREF_RSS_FEED = "pref_rss_feed";
    public static final String PREF_RSS_PAGE = "pref_rss_page";

    private int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private Intent resultIntent;
    private EditText edtRSSAddress;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        resultIntent = new Intent();
        resultIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        setResult(RESULT_CANCELED, resultIntent);

        sharedPreferences = getSharedPreferences(WIDGET_SETTINGS, MODE_PRIVATE);
        setContentView(R.layout.activity_settings);
        edtRSSAddress = (EditText) findViewById(R.id.edt_rss_address);
        String address = sharedPreferences.getString(SettingsActivity.PREF_RSS_ADDRESS + widgetId, "");
        edtRSSAddress.setText(address);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                String address = edtRSSAddress.getText().toString();
                String regex = "\\b(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
                if(Pattern.compile(regex).matcher(address).matches()){
                    setResult(RESULT_OK, resultIntent);
                    sharedPreferences.edit().putString(PREF_RSS_ADDRESS + widgetId, address).apply();
                    Intent intent = new Intent(getApplicationContext(), WidgetProvider.class);
                    intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                    getApplicationContext().sendBroadcast(intent);
                    finish();
                }else{
                    edtRSSAddress.setError(getResources().getString(R.string.wrong_input));
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
