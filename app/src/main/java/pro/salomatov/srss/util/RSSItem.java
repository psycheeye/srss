package pro.salomatov.srss.util;

/**
 * Created by NASTYA on 15.10.2015.
 */
public class RSSItem {

    private String title;
    private String description;

    public RSSItem() {
        this.title = "";
        this.description = "";
    }

    public RSSItem(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
